package com.openvpn.library.core;

import com.openvpn.library.core.LogItem;
import com.openvpn.library.core.ConnectionStatus;



interface IStatusCallbacks {

    oneway void newLogItem(in LogItem item);

    oneway void updateStateString(in String state, in String msg, in int resid, in ConnectionStatus level);

    oneway void updateByteCount(long inBytes, long outBytes);

    oneway void connectedVPN(String uuid);
}
