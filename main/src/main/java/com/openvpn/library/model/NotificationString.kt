package com.openvpn.library.model

import androidx.annotation.StringRes


/**
 * Created by Roman K. on 13.09.2019.
 * Copyright (c) 2019 ROMAN KONONOVICH. All rights reserved.
 */
data class NotificationString(
    /**
     *
     <string name="state_connecting">OpenVPN - %s</string>
     <string name="state_connecting">Connecting</string>
    <string name="state_wait">Waiting for server reply</string>
    <string name="state_auth">Authenticating</string>
    <string name="state_get_config">Getting client configuration</string>
    <string name="state_assign_ip">Assigning IP addresses</string>
    <string name="state_add_routes">Adding routes</string>
    <string name="state_connected">Connected</string>
    <string name="state_disconnected">Disconnect</string>
    <string name="state_reconnecting">Reconnecting</string>
    <string name="state_exiting">Exiting</string>
    <string name="state_noprocess">Not running</string>
    <string name="state_resolve">Resolving host names</string>
    <string name="state_tcp_connect">Connecting (TCP)</string>
    <string name="state_auth_failed">Authentication failed</string>
    <string name="state_auth_pending">Authentication pending</string>
    <string name="state_nonetwork">Waiting for usable network</string>
    <string name="state_waitorbot">Waiting for Orbot to start</string>
    <string name="notifcation_title_notconnect">Not connected</string>
     */

    @StringRes
    val notificationAppName: Int,
    @StringRes
    val stateConnecting: Int,
    @StringRes
    val stateWait: Int,
    @StringRes
    val stateAuth: Int,
    @StringRes
    val stateGetConfig: Int,
    @StringRes
    val stateAssignIp: Int,
    @StringRes
    val stateAddRoutes: Int,
    @StringRes
    val stateConnected: Int,
    @StringRes
    val stateDisconnected: Int,
    @StringRes
    val stateReconnecting: Int,
    @StringRes
    val stateExiting: Int,
    @StringRes
    val stateNoProcess: Int,
    @StringRes
    val stateResolve: Int,
    @StringRes
    val stateTCPConnect: Int,
    @StringRes
    val stateAuthFailed: Int,
    @StringRes
    val stateAuthPending: Int,
    @StringRes
    val stateNoNetwork: Int,
    @StringRes
    val stateWaitOrBot: Int,
    @StringRes
    val NotificationTitleNotConnect: Int
)