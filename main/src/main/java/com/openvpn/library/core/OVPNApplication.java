package com.openvpn.library.core;

import android.annotation.TargetApi;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.StrictMode;

import com.openvpn.library.BuildConfig;
import com.openvpn.library.R;
import com.openvpn.library.api.AppRestrictions;
import com.openvpn.library.model.NotificationString;

public class OVPNApplication extends Application {
    private StatusListener mStatus;


    static int notificationResIconId() {
        return notificationIcon;
    }

    private static int notificationIcon = android.R.drawable.btn_star;



    static NotificationString notificationStringsArray() {
        return notificationStringsArray;
    }

    private static NotificationString notificationStringsArray = new NotificationString(
            R.string.app,
            R.string.state_connecting,
            R.string.state_wait,
            R.string.state_auth,
            R.string.state_get_config,
            R.string.state_assign_ip,
            R.string.state_add_routes,
            R.string.state_connected,
            R.string.state_disconnected,
            R.string.state_reconnecting,
            R.string.state_exiting,
            R.string.state_noprocess,
            R.string.state_resolve,
            R.string.state_tcp_connect,
            R.string.state_auth_failed,
            R.string.state_auth_pending,
            R.string.state_nonetwork,
            R.string.state_waitorbot,
            R.string.notifcation_title_notconnect);


    public void setOVPNNotificationIcon(int resIdIcon) {
        notificationIcon = resIdIcon;
    }


    public void setOVPNNotificationStringsArray(NotificationString notificationString) {
        notificationStringsArray = notificationString;
    }

    @Override
    public void onCreate() {
        if ("robolectric".equals(Build.FINGERPRINT))
            return;

        super.onCreate();
        PRNGFixes.apply();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            createNotificationChannels();
        mStatus = new StatusListener();
        mStatus.init(getApplicationContext());

        if (BuildConfig.BUILD_TYPE.equals("debug"))
            enableStrictModes();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AppRestrictions.getInstance(this).checkRestrictions(this);
        }
    }

    private void enableStrictModes() {
        StrictMode.VmPolicy policy = new StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeath()
                .build();
        StrictMode.setVmPolicy(policy);

    }


    @TargetApi(Build.VERSION_CODES.O)
    private void createNotificationChannels() {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Background message
        CharSequence name = getString(R.string.channel_name_background);
        NotificationChannel mChannel = new NotificationChannel(OpenVPNService.NOTIFICATION_CHANNEL_BG_ID,
                name, NotificationManager.IMPORTANCE_MIN);

        mChannel.setDescription(getString(R.string.channel_description_background));
        mChannel.enableLights(false);

        mChannel.setLightColor(Color.DKGRAY);
        mNotificationManager.createNotificationChannel(mChannel);

        // Connection status change messages

        name = getString(R.string.channel_name_status);
        mChannel = new NotificationChannel(OpenVPNService.NOTIFICATION_CHANNEL_NEWSTATUS_ID,
                name, NotificationManager.IMPORTANCE_LOW);

        mChannel.setDescription(getString(R.string.channel_description_status));
        mChannel.enableLights(true);

        mChannel.setLightColor(Color.BLUE);
        mNotificationManager.createNotificationChannel(mChannel);


        // Urgent requests, e.g. two factor auth
        name = getString(R.string.channel_name_userreq);
        mChannel = new NotificationChannel(OpenVPNService.NOTIFICATION_CHANNEL_USERREQ_ID,
                name, NotificationManager.IMPORTANCE_HIGH);
        mChannel.setDescription(getString(R.string.channel_description_userreq));
        mChannel.enableVibration(true);
        mChannel.setLightColor(Color.CYAN);
        mNotificationManager.createNotificationChannel(mChannel);
    }
}
