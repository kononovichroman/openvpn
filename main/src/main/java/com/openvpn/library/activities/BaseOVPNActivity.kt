package com.openvpn.library.activities

import android.app.Activity
import android.content.*
import android.net.Uri
import android.net.VpnService
import android.os.Build
import android.os.IBinder
import android.os.RemoteException
import android.provider.OpenableColumns
import com.openvpn.library.LaunchVPN
import com.openvpn.library.R
import com.openvpn.library.VpnProfile
import com.openvpn.library.api.ExternalAppDatabase
import com.openvpn.library.core.*
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader


abstract class BaseOVPNActivity : Activity() {


    fun doImportUri(data: Uri, serverName: String) {
        var possibleName: String? = null
        if (data.scheme != null && data.scheme == "file" || data.lastPathSegment != null && (data.lastPathSegment!!.endsWith(
                ".ovpn"
            ) || data.lastPathSegment!!.endsWith(".conf"))
        ) {
            possibleName = data.lastPathSegment
            if (possibleName!!.lastIndexOf('/') != -1)
                possibleName = possibleName.substring(possibleName.lastIndexOf('/') + 1)

        }


        val cursor = this.contentResolver.query(data, null, null, null, null)

        cursor.use {
            if (it != null && it.moveToFirst()) {
                var columnIndex = it.getColumnIndex(OpenableColumns.DISPLAY_NAME)

                if (columnIndex != -1) {
                    val displayName = it.getString(columnIndex)
                    if (displayName != null)
                        possibleName = displayName
                }
                columnIndex = it.getColumnIndex("mime_type")
                if (columnIndex != -1) {
                    //                    log("Mime type: " + cursor!!.getString(columnIndex))
                }
            }
        }
        if (possibleName != null) {
            possibleName = possibleName?.replace(".ovpn", "")
            possibleName = possibleName?.replace(".conf", "")
        }

        startImportTask(data, possibleName, serverName)


    }

    private fun startImportTask(data: Uri, possibleName: String?, serverName: String) {
        val inputStream = this.contentResolver.openInputStream(data)
        inputStream?.let { doImport(it, serverName) }
    }

    private fun doImport(inputStream: InputStream, serverName: String) {
        val cp = ConfigParser()
        try {
            val isr = InputStreamReader(inputStream)

            cp.parseConfig(isr)
            vpnProfile = cp.convertProfile()
            saveProfile(serverName)
//            embedFiles(cp)
            return

        } catch (e: IOException) {
//            log(R.string.error_reading_config_file)
//            log(e.localizedMessage)
        } catch (e: ConfigParser.ConfigParseError) {
//            log(R.string.error_reading_config_file)
//            log(e.localizedMessage)
        } finally {
            inputStream.close()
        }

        vpnProfile = null

    }

    companion object {
        var vpnProfile: VpnProfile? = null
        var time: Long? = null
    }

    private fun saveProfile(serverName: String) {


        val vpl = ProfileManager.getInstance(this)
        vpnProfile?.mName = serverName
        val temp = vpl.profiles
        temp.forEach {
            vpl.profiles.remove(it)
        }

        vpl.addProfile(vpnProfile)

        vpl.saveProfile(this, vpnProfile)
        vpl.saveProfileList(this)
        vpnProfile?.let { startOrStopVPN(it) }
    }

    private fun startOrStopVPN(profile: VpnProfile) {
        if (VpnStatus.isVPNActive()) {
            ProfileManager.setConntectedVpnProfileDisconnected(this)
            if (vpnService != null) {
                try {
                    vpnService?.stopVPN(false)
                } catch (e: RemoteException) {
                    VpnStatus.logException(e)
                }

            }
        } else {
            startVPN(profile)
        }
    }

    override fun onResume() {
        super.onResume()
        val intent = Intent(this, OpenVPNService::class.java)
        intent.action = OpenVPNService.START_SERVICE
        bindService(intent, connection, Context.BIND_AUTO_CREATE)
    }

    private var selectedProfile: VpnProfile? = null
    private var vpnService: IOpenVPNServiceInternal? = null
    private val connection = object : ServiceConnection {
        override fun onServiceConnected(
            className: ComponentName,
            service: IBinder
        ) {
            vpnService = IOpenVPNServiceInternal.Stub.asInterface(service)
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            vpnService = null
        }

    }

    override fun onPause() {
        super.onPause()
        unbindService(connection)

    }

    private fun startVPN(profile: VpnProfile) {

        ProfileManager.getInstance(this).saveProfile(this, profile)

//        val intent = Intent()
        val intent = Intent(this, LaunchVPN::class.java)
        intent.putExtra(LaunchVPN.EXTRA_KEY, profile.uuid.toString())
        intent.action = Intent.ACTION_MAIN
//        startVpnFromIntent(intent)
        startActivity(intent)
    }

    private fun startVpnFromIntent(intent: Intent) {
        // Resolve the intent
        val action = intent.action

        if (Intent.ACTION_MAIN == action) {
            if (Preferences.getDefaultSharedPreferences(this).getBoolean(
                    LaunchVPN.CLEARLOG,
                    true
                )
            )
                VpnStatus.clearLog()

            val shortcutUUID = intent.getStringExtra(LaunchVPN.EXTRA_KEY)
            val shortcutName = intent.getStringExtra(LaunchVPN.EXTRA_NAME)

            var profileToConnect: VpnProfile? = ProfileManager.get(this, shortcutUUID)
            if (shortcutName != null && profileToConnect == null) {
                profileToConnect =
                    ProfileManager.getInstance(this).getProfileByName(shortcutName)
                if (!ExternalAppDatabase(this).checkRemoteActionPermission(
                        this,
                        this.callingPackage
                    )
                ) {
//                    finish()
                    return
                }
            }


            if (profileToConnect == null) {
                VpnStatus.logError(R.string.shortcut_profile_notfound)
            } else {
                selectedProfile = profileToConnect
                launchVPN()
            }
        }
    }

    private fun launchVPN() {
        val vpnok = selectedProfile?.checkProfile(this)
        if (vpnok != R.string.no_error_found) {
//            showConfigErrorDialog(vpnok)
            return
        }

        val intent = VpnService.prepare(this)
        // Check if we want to fix /dev/tun
        val prefs = Preferences.getDefaultSharedPreferences(this)
        val usecm9fix = prefs.getBoolean("useCM9Fix", false)
        val loadTunModule = prefs.getBoolean("loadTunModule", false)

//        if (loadTunModule)
//            LaunchVPN.execeuteSUcmd("insmod /system/lib/modules/tun.ko")
//
//        if (usecm9fix && !LaunchVPN().mCmfixed) {
//            LaunchVPN().execeuteSUcmd("chown system /dev/tun")
//        }

        if (intent != null) {
            VpnStatus.updateStateString(
                "USER_VPN_PERMISSION", "", R.string.state_user_vpn_permission,
                ConnectionStatus.LEVEL_WAITING_FOR_USER_INPUT
            )
            // Start the query
            try {
                startActivityForResult(intent, LaunchVPN.START_VPN_PROFILE)
            } catch (ane: ActivityNotFoundException) {
                // Shame on you Sony! At least one user reported that
                // an official Sony Xperia Arc S image triggers this exception
                VpnStatus.logError(R.string.no_vpn_support_image)
//                showLogWindow()
            }

        } else {
            onActivityResult(LaunchVPN.START_VPN_PROFILE, Activity.RESULT_OK, null)
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == LaunchVPN.START_VPN_PROFILE) {
            if (resultCode == Activity.RESULT_OK) {
                ProfileManager.updateLRU(this, selectedProfile)
                VPNLaunchHelper.startOpenVpn(selectedProfile, this)
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // User does not want us to start, so we just vanish
                VpnStatus.updateStateString(
                    "USER_VPN_PERMISSION_CANCELLED",
                    "",
                    R.string.state_user_vpn_permission_cancelled,
                    ConnectionStatus.LEVEL_NOTCONNECTED
                )

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    VpnStatus.logError(R.string.nought_alwayson_warning)
            }
        }
    }
}