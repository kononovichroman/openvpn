plugins {
    id("com.android.library")
    id("checkstyle")
}

apply {
    plugin("kotlin-android")
    plugin("kotlin-android-extensions")
}

repositories {
    jcenter()
    maven(url = "https://jitpack.io")
    google()
}

dependencies {
    implementation("androidx.annotation:annotation:1.1.0")
}


val openvpn3SwigFiles = File(buildDir, "generated/source/ovpn3swig/ovpn3")

tasks.register<Exec>("generateOpenVPN3Swig")
{
    var swigcmd = "swig"
    // Workaround for Mac OS X since it otherwise does not find swig and I cannot get
    // the Exec task to respect the PATH environment :(
    if (File("/usr/local/bin/swig").exists())
        swigcmd = "/usr/local/bin/swig"

    doFirst {
        mkdir(openvpn3SwigFiles)
    }
    commandLine(
        listOf(
            swigcmd,
            "-outdir",
            openvpn3SwigFiles,
            "-outcurrentdir",
            "-c++",
            "-java",
            "-package",
            "net.openvpn.ovpn3",
            "-Isrc/main/cpp/openvpn3/client",
            "-Isrc/main/cpp/openvpn3/",
            "-o",
            "${openvpn3SwigFiles}/ovpncli_wrap.cxx",
            "-oh",
            "${openvpn3SwigFiles}/ovpncli_wrap.h",
            "src/main/cpp/openvpn3/javacli/ovpncli.i"
        )
    )
}

android {
    compileSdkVersion(29)

    defaultConfig {
        //        minSdkVersion(14)
        minSdkVersion(21)
        targetSdkVersion(29)  //'Q'.toInt()
        versionCode = 161
        versionName = "0.7.8"

        externalNativeBuild {
            cmake {
                //arguments = listOf("-DANDROID_TOOLCHAIN=clang",
                //        "-DANDROID_STL=c++_static")
                //abiFilters "arm64-v8a"
            }
        }
    }

    externalNativeBuild {
        cmake {
            setPath(File("${projectDir}/src/main/cpp/CMakeLists.txt"))
        }
    }

    sourceSets {
        getByName("main") {
            assets.srcDirs("src/main/assets", "build/ovpnassets")
        }

        create("normal") {
            java.srcDirs("src/ovpn3/java/", openvpn3SwigFiles)
        }

        getByName("debug") {

        }

        getByName("release") {

        }
    }

    signingConfigs {
        create("release") {}
    }

//    lintOptions {
//        enable("BackButton", "EasterEgg", "StopShip", "IconExpectedSize", "GradleDynamicVersion", "NewerVersionAvailable")
//        warning("ImpliedQuantity", "MissingQuantity")
//        disable("MissingTranslation", "UnsafeNativeCodeLocation")
//    }

    flavorDimensions("implementation")

    productFlavors {
        /*create("noovpn3") {
            setDimension("implementation")
            buildConfigField ("boolean", "openvpn3", "false")
        }*/
        create("normal") {
            setDimension("implementation")
            buildConfigField("boolean", "openvpn3", "true")
        }

    }


    compileOptions {
        targetCompatibility = JavaVersion.VERSION_1_8
        sourceCompatibility = JavaVersion.VERSION_1_8
    }
}

/* Hack-o-rama but it works good enough and documentation is surprisingly sparse */

val swigTask = tasks.named("generateOpenVPN3Swig")
val preBuildTask = tasks.getByName("preBuild")
val assembleTask = tasks.getByName("assemble")

println(tasks.names)

assembleTask.dependsOn(swigTask)
preBuildTask.dependsOn(swigTask)

afterEvaluate {
    val debugFile = file("$buildDir/outputs/aar/main-normal-release.aar")
    tasks.named("assembleRelease").configure {
        doLast {
            val finalFolder = file("$rootDir/library")
            if (!finalFolder.exists()) {
                finalFolder.mkdirs()
            }
            val finalFile = file("$rootDir/library/ovpn-library.aar")
            finalFile.writeBytes(debugFile.readBytes())
        }
    }
}