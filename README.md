[![ version](https://img.shields.io/badge/min%20API%20level-21-green)](https://developer.android.com/studio/releases/platforms#5.0) [![ last-commit-ics-openvpn](https://img.shields.io/badge/commit--ics--openvpn-ae1b90bd6b279dfaf7f7aaf87a2022fc23015808-blue)](https://github.com/schwabe/ics-openvpn/commit/ae1b90bd6b279dfaf7f7aaf87a2022fc23015808)

### Adding library in Android studio

1. In AS:
**File** -> **New** -> **New Module..** -> **Import .JAR/.AAR Package** -> **Next** -> choose [**ovpn-library.aar**](/library/ovpn-library.aar) and click **finish**.

2. In the **dependencies** (**build.gradle** (application)) section add the following line:

```
implementation project(":ovpn-library")
```

and click "**Sync Now**"



### The library configuration:

1. Create Application file that extends the **OVPNApplication()** and in **onCreate()** : 

   - set up the icon for notifications:

```
setOVPNNotificationIcon(@DrawableRes)
```

   - set up strings for notifications:

```
setOVPNNotificationStringsArray(
       NotificationString(@StringRes)
   )
```

and register Application class in Manifest:

```xml
<application
    ...
	android:name=".App"
    ...>
</application>
```



### Use of the library:

1. Create Activity file that extends the **BaseOVPNActivity()** and make the connection:

```
doImportUri(
       Uri.fromFile(File(getExternalFilesDir(null), "config.ovpn")),
       serverName
   )
```

2. If you need check state connection register **VPNConnectBroadcastReceiver** in your Activity:

```
private fun registerReceiverService() {
       vpnBroadcastReceiver = VPNConnectBroadcastReceiver()
       val intentFilter = IntentFilter("com.openvpn.library.VPN_STATUS")
       intentFilter.addCategory(Intent.CATEGORY_DEFAULT)
       registerReceiver(vpnBroadcastReceiver, intentFilter)
   }
     
   inner class VPNConnectBroadcastReceiver : BroadcastReceiver() {
           override fun onReceive(context: Context, intent: Intent) {
               val status = intent.extras?.getString("status")
               val detailstatus = intent.extras?.getString("detailstatus")
           }
       }
    override fun onStop() {
           super.onStop()
           unregisterReceiver(vpnBroadcastReceiver)
       }
```